	object recursive{

		def main(args:Array[String]){


			println("Enter your number=");
			var m=scala.io.StdIn.readInt();

			var check=Prime(m, m/2);

			if(check==1){

				println("true.");
			}
			else{

				println("false.");
			}


		}

		def Prime(n:Int, i:Int):Int={

				if(i==1){

					return 1;
				}
				else{
					if(n%i==0){
						return 0;
					}
					else{

						return Prime(n,i-1);
					}
				}
				

		}

	}