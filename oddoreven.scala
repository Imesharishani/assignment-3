	object oddoreven{

		def main(args:Array[String]){


			println("Enter your number=");
			var m=scala.io.StdIn.readInt();

			var check=A(m,2);

			if(check==1){

				print(m);
				println(" is a ODD number.");
			}
			else{

				print(m);
				println(" is a EVEN number.");
			}

		}

		def A(n:Int,x:Int):Int={


				if(n==1){

					return 1;
				}
				else{
					if(n%x==0){
						return 0;
					}
					else{

						return A(n-(n-1),2);
					}
				}

		}

	}