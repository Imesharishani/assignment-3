	object recursive2{

		def main(args:Array[String]){


			println("Enter your number=");
			var m=scala.io.StdIn.readInt();
			
			println(Primeseq(m));

			}


		def gcd(x:Int, y:Int):Int=y match {
			case 0 => x
			case y if(y>x) =>gcd(y,x);
			case default =>gcd(y,x%y);

		}

		def prime(s:Int, t:Int=2):Boolean=t match {
			case p if(p==s)=> true
			case p if(gcd(s,p)>1) =>false
			case p =>prime(s,t+1)
		}


		def Primeseq(n:Int):Unit={

			if(prime(n)){
				println(n)
			}
			if(n>0){

				Primeseq(n-1);
			}

		}
	}